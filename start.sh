#!/bin/sh -e


if [ -z "$WSREP_CLUSTER_ADDRESS" -a -n "$CLUSTER_PORT_4567_TCP_ADDR" ]; then
	set -- "$@" --wsrep_cluster_address=gcomm://"$CLUSTER_PORT_4567_TCP_ADDR"
	echo set wsrep_cluster_address is \"gcomm://"$CLUSTER_PORT_4567_TCP_ADDR"\"
fi
if [ -n "$WSREP_CLUSTER_ADDRESS" ]; then
	set -- "$@" --wsrep_cluster_address="$WSREP_CLUSTER_ADDRESS"
	echo set wsrep_cluster_address is "$WSREP_CLUSTER_ADDRESS"
fi

if [ -n "$WSREP_NODE_NAME" ]; then
	set -- "$@" --wsrep_node_name="$WSREP_NODE_NAME"
	echo set wsrep_node_name is "$WSREP_NODE_NAME"
fi

if [ -n "$WSREP_SST_AUTH" ]; then
	set -- "$@" --wsrep_sst_auth="$WSREP_SST_AUTH"
	echo set wsrep_sst_auth is "$WWSREP_SST_AUTH"
fi

if [ -n "$NEW_CLUSTER" -a "$NEW_CLUSTER" = "true" ]; then
	set -- "$@" --wsrep-new-cluster
fi

if [ -n "$NODE_PORT_3306_TCP_ADDR" ]; then
	set -- "$@" --wsrep_node_address="$NODE_PORT_3306_TCP_ADDR"
	echo set wsrep_node_address is "$NODE_PORT_3306_TCP_ADDR"
fi

if [ -n "$WSREP_SST_METHOD" ]; then
	set -- "$@" --wsrep_sst_method="$WSREP_SST_METHOD"
	echo set wsrep_sst_method is "$WSREP_SST_METHOD"
fi

if [ -n "$WSREP_SLAVE_THREADS" ]; then
	set -- "$@" --wsrep_slave_threads="$WSREP_SLAVE_THREADS"
	echo set wsrep_slave_threads is "$WSREP_SLAVE_THREADS"
fi

if [ ! -d '/var/lib/mysql/mysql' -a "${1%_safe}" = 'mysqld' ]; then
	if [ -z "$MYSQL_ROOT_PASSWORD" ]; then
		echo >&2 'error: database is uninitialized and MYSQL_ROOT_PASSWORD not set'
		echo >&2 '  Did you forget to add -e MYSQL_ROOT_PASSWORD=... ?'
		exit 1
	fi
	mysql_install_db --user=mysql --datadir=/var/lib/mysql
	
	TEMP_FILE='/tmp/mysql-first-time.sql'
	
	cat > "$TEMP_FILE" <<-EOSQL
		DELETE FROM mysql.user ;
		CREATE USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}' ;
		GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;
		DROP DATABASE IF EXISTS test ;
	EOSQL
	
	if [ "$MYSQL_DATABASE" ]; then
		echo "CREATE DATABASE IF NOT EXISTS $MYSQL_DATABASE ;" >> "$TEMP_FILE"
	fi
	
	if [ "$MYSQL_USER" -a "$MYSQL_PASSWORD" ]; then
		echo "CREATE USER '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD' ;" >> "$TEMP_FILE"
		
		if [ "$MYSQL_DATABASE" ]; then
			echo "GRANT ALL ON $MYSQL_DATABASE.* TO '$MYSQL_USER'@'%' ;" >> "$TEMP_FILE"
		fi
	fi
	
	echo 'FLUSH PRIVILEGES ;' >> "$TEMP_FILE"
	
	set -- "$@" --init-file="$TEMP_FILE"
fi

chown -R mysql:mysql /var/lib/mysql
echo "$@"
exec "$@"
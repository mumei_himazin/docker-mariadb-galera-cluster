FROM ubuntu:14.04.1

RUN apt-get update && apt-get -y upgrade && apt-get clean
RUN apt-get -y install software-properties-common \
	&& apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db \
	&& add-apt-repository 'deb http://ftp.yz.yamagata-u.ac.jp/pub/dbms/mariadb/repo/5.5/ubuntu trusty main' \
	&& apt-get update \
	&& apt-get -y install mariadb-galera-server galera mariadb-client rsync xtrabackup
	
ADD cluster.cnf /etc/mysql/conf.d/cluster.cnf
RUN mv /etc/mysql/conf.d/mariadb.cnf /etc/mysql/conf.d/mariadb.cnf.back
ADD mariadb.cnf /etc/mysql/conf.d/mariadb.cnf
ADD start.sh /start.sh
RUN chmod 700 /start.sh

WORKDIR /usr/local/mysql
VOLUME /var/lib/mysql
ENV TERM xterm

ENTRYPOINT ["/start.sh"]

EXPOSE 3306 4567 4568 4444

CMD ["mysqld", "--datadir=/var/lib/mysql", "--user=mysql"]
#docker-mariadb-galera-cluster

## installed

- ubuntu 14.04.1
- mariadb-galera-server
- galera
- xtrabackup

## Build
	docker build --rm -t <image_name> docker-mariadb-galera-cluster

## Usage
####初期ノード  
	docker run -d -p <port>:3306 -v <db files dir>:/var/lib/mysql:rw \
	-e NEW_CLUSTER=true \
	-e MYSQL_ROOT_PASSWORD=<password> \
	-e WSREP_NODE_NAME=<node0> \
	-e "WSREP_SST_AUTH=<user>:<password>" \
	--name <name0> <image_name>

####その他ノード  
	docker run -d -p <port>:3306 -v <db files dir>:/var/lib/mysql:rw \
	--link <name0>:cluster \
	-e MYSQL_ROOT_PASSWORD=<password> \
	-e WSREP_NODE_NAME=<node1> \
	-e "WSREP_SST_AUTH=<user>:<password>" \
	--name <name1> <image_name>

##sample
	mkdir /var/db /var/db/0 /var/db/1 /var/db/2
	chown -R messagebus:fuse /var/db
	docker build -t maria --rm docker-mariadb-galera-cluster
	
	docker run -d -p 3308:3306 -v /var/db/0:/var/lib/mysql:rw -e NEW_CLUSTER=true -e MYSQL_ROOT_PASSWORD=test -e WSREP_NODE_NAME=node0 -e "WSREP_SST_AUTH=root:test"  --name maria0 maria

	docker run -d -p 3309:3306 -v /var/db/1:/var/lib/mysql:rw --link maria0:cluster -e MYSQL_ROOT_PASSWORD=test -e WSREP_NODE_NAME=node1 -e "WSREP_SST_AUTH=root:test"  --name maria1 maria
	docker run -d -p 3310:3306 -v /var/db/2:/var/lib/mysql:rw --link maria0:cluster -e MYSQL_ROOT_PASSWORD=test -e WSREP_NODE_NAME=node2 -e "WSREP_SST_AUTH=root:test"  --name maria2 maria